package ChangeMachine;

public class Machine {


    private double articlePrice;
    private double amountReceived;

    public Machine( double articlePrice, double amountReceived){
        this.articlePrice = articlePrice;
        this.amountReceived = amountReceived;
        //System.out.println("hello from the machine constructor");
    }

    public void cashierResponse( double articlePrice, double amountReceived){
        //System.out.println("from the cashierResponse");
        if(articlePrice == amountReceived){
            System.out.println("article = amount");
            System.out.println(Messages.NO_CHANGE.getMessage());

        }
        else if( articlePrice > amountReceived) {
            System.out.println(Messages.MORE_MONEY.getMessage());
        }
        else {
            System.out.println(calculateChange(articlePrice, amountReceived));
        }

    }

    public String calculateChange( double articlePrice, double amountReceived){
        double changeAmount = amountReceived - articlePrice;

        // calculate the euro change
        String stringEuroResult = Double.toString(changeAmount);
        String stringEuroResult2= stringEuroResult.substring(0,stringEuroResult.indexOf('.'));
        double euroResult = Double.valueOf(stringEuroResult2);
        
        // calculate the cents
        String stringCentsResult= Double.toString(changeAmount);
        String stringCentsResult2= stringCentsResult.substring(stringCentsResult.indexOf('.'),stringCentsResult.length());
        double centsResult = Double.valueOf(stringCentsResult2);

        /*
        // count the number of bankNotes that are necessary.(units)
        double numberOfBankNotes=0;
        while( euroResult >= 5) {
            for (BankNotes bn : BankNotes.values()) {
                if (bn.getCurrency() == euroResult || bn.getCurrency() < euroResult) {
                   euroResult = euroResult - bn.getCurrency();
                    numberOfBankNotes = numberOfBankNotes + 1;
                }
                double newEuroResult = euroResult;
            }
        }

        // count the number of coins that are necessary( units)
        double numberOfCoinsEuro = 0;
        while(euroResult < 5){
            for( Coins c :Coins.values()){
                if( c.getCurrency() == euroResult || c.getCurrency() < euroResult){
                    double newEuroResult = euroResult - c.getCurrency();
                    numberOfCoinsEuro = numberOfCoinsEuro + 1;
                }
            }
        }

        // count the coins (cents)
        double numberOfCoinsCents = 0;
        while(centsResult <0.99){
            for(Coins c : Coins.values()){
                if(c.getCurrency()== centsResult|| c.getCurrency() < centsResult ){
                    double NewCentsResult = centsResult - c.getCurrency();
                    numberOfCoinsCents = numberOfCoinsCents + 1;
                }
            }
        }
*/
        return "The change is:" + changeAmount + " €(" + euroResult +" €,"+ centsResult+" cents.)."; //+ newEuroResult + numberOfBankNotes;
    }
}
