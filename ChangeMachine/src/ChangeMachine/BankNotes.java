package ChangeMachine;

public enum BankNotes {

    FIFTY_EUROS(50),
    TWENTY_EUROS(20),
    TEN_EUROS(10),
    FIVE_EUROS(5);


    private double currency;
    BankNotes(double currency){
        this.currency = currency;
    }

    public double getCurrency(){
        return this.currency;
    }
}
