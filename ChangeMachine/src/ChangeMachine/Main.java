package ChangeMachine;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // input from the price of the article;
        Scanner articlePriceQuestion =new Scanner(System.in);
        System.out.println(Messages.ARTICLE_PRICE.getMessage() + ":");

        double articlePrice = articlePriceQuestion.nextDouble();
        System.out.println(Messages.ARTICLE_PRICE.getMessage() +":"+  articlePrice +" €.");


        // input from the amount received;
        Scanner amountReceivedQuestion = new Scanner(System.in);
        System.out.println(Messages.AMOUNT_RECEIVED.getMessage() +":");

        double amountReceived = amountReceivedQuestion.nextDouble();
        System.out.println(Messages.AMOUNT_RECEIVED.getMessage() + " : "+ amountReceived+ " €");

        // create the change machine and give it the inputs;
        Machine changeMachine = new Machine(articlePrice,amountReceived);
        changeMachine.cashierResponse(articlePrice,amountReceived);
    }

}
