package ChangeMachine;

public enum Messages {
    ARTICLE_PRICE("Article price"),
    AMOUNT_RECEIVED("Amount received"),
    NO_CHANGE("Does not need change"),
    MORE_MONEY("You need to give more money"),
    CHANGE("The change is:")
    ;



    private String message;
    Messages(String message) {
        this.message =message;
    }

    public String getMessage() {
        return message;
    }
}
