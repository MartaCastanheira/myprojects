package ChangeMachine;

public enum Coins {

    TWO_EURO(2),
    ONE_EURO(1),
    FIFTY_CENTS(0.5),
    TWENTY_CENTS(0.2),
    TEN_CENTS(0.1),
    FIVE_CENTS(0.05),
    ONE_CENT(0.01)
    ;


    private double currency;

 Coins(double currency){
     this.currency = currency;
 }

    public double getCurrency() {
        return currency;
    }
}
